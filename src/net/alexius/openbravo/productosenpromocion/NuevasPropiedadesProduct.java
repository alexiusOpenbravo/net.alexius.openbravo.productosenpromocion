package net.alexius.openbravo.productosenpromocion;

import java.util.ArrayList;
import java.util.List;

import org.openbravo.client.kernel.ComponentProvider.Qualifier;
import org.openbravo.mobile.core.model.HQLProperty;
import org.openbravo.mobile.core.model.ModelExtension;
import org.openbravo.retail.posterminal.master.Product;

@Qualifier(Product.productPropertyExtension)
public class NuevasPropiedadesProduct extends ModelExtension {

  @Override
  public List<HQLProperty> getHQLProperties(Object params) {
    List<HQLProperty> properties = new ArrayList<HQLProperty>();
    properties.add(new HQLProperty("alprom_has_promotion(product.id)", "alprom_qty"));
    return properties;
  }
}