package net.alexius.openbravo.productosenpromocion;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;

import org.apache.log4j.Logger;
import org.openbravo.client.kernel.BaseComponentProvider;
import org.openbravo.client.kernel.BaseComponentProvider.ComponentResource.ComponentResourceType;
import org.openbravo.client.kernel.Component;
import org.openbravo.client.kernel.ComponentProvider;
import org.openbravo.retail.posterminal.POSUtils;

@ApplicationScoped
@ComponentProvider.Qualifier(ALPROMComponentProvider.QUALIFIER)
public class ALPROMComponentProvider extends BaseComponentProvider {
  public static final String QUALIFIER = "ALPROM_main";
  public static final String MODULE_JAVA_PACKAGE = "net.alexius.openbravo.productosenpromocion";
  Logger log = Logger.getLogger(ALPROMComponentProvider.class);

  @Override
  public Component getComponent(String componentId, Map<String, Object> parameters) {
    throw new IllegalArgumentException("Component id " + componentId + " not supported.");
  }

  @Override
  public List<ComponentResource> getGlobalComponentResources() {
    final GlobalResourcesHelper grhelper = new GlobalResourcesHelper();
   
    //SRAMIREZ
    grhelper.add("models/nuevasPropiedadesProduct.js");
    grhelper.add("components/PromDescriptionPopUp.js");
    grhelper.add("hooks/PreAddProductToOrderHook.js");
    grhelper.add("hooks/RenderProductHook.js");
    return grhelper.getGlobalResources();
  }

  private class GlobalResourcesHelper {
    private final List<ComponentResource> globalResources = new ArrayList<ComponentResource>();
    private final String prefix = "web/" + MODULE_JAVA_PACKAGE + "/js/";

    public void add(String file) {
      globalResources.add(
          createComponentResource(ComponentResourceType.Static, prefix + file, POSUtils.APP_NAME));
    }
    public List<ComponentResource> getGlobalResources() {
      return globalResources;
    }
  }
}
