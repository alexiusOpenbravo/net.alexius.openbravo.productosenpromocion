package net.alexius.openbravo.productosenpromocion;
import java.util.Arrays;
import java.util.List;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.mobile.core.process.ProcessHQLQuery;


public class GetProductPromotions extends ProcessHQLQuery {

  @Override
 protected boolean isAdminMode() {
   return true;
 }

 @Override
 protected List<String> getQuery(JSONObject jsonsent) throws JSONException {
   return Arrays
       .asList(new String[] {"select name, alpromPosmessage from PricingAdjustment where alprom_prom_product(:idProducto,id)>0"});
       
    }
}