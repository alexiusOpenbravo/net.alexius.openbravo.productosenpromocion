package net.alexius.openbravo.productosenpromocion;

import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;

import org.hibernate.dialect.function.SQLFunction;
import org.hibernate.dialect.function.SQLFunctionTemplate;
import org.hibernate.dialect.function.StandardSQLFunction;
import org.hibernate.type.StandardBasicTypes;
import org.openbravo.dal.core.SQLFunctionRegister;
import org.openbravo.service.db.DalConnectionProvider;

/**
Clase encargada de registrar la funcion 
 */
@ApplicationScoped
public class ALPROMSQLFunctionRegister implements SQLFunctionRegister {

  @Override
  public Map<String, SQLFunction> getSQLFunctions() {
    Map<String, SQLFunction> sqlFunctions = new HashMap<>();
    sqlFunctions.put("alprom_has_promotion",
        new StandardSQLFunction("alprom_has_promotion", StandardBasicTypes.INTEGER));
    sqlFunctions.put("alprom_prom_product",
        new StandardSQLFunction("alprom_prom_product", StandardBasicTypes.INTEGER));
    return sqlFunctions;
  }
}
