enyo.kind({
    kind: 'OB.UI.ModalAction',
    name: 'ALPROM_DescripcionPromocion',
    bodyContent: {
        maxHeight: '250px',
        components: [{
            name: 'message1',
            style: 'color: #333333; width: 100%; font-size: 20px; background-color: white;text-align:left',
            allowHtml:true
        }, {
            name: 'labelProductosNotificacion',
            content: ''
        }]
    },
    bodyButtons: {
        components: [{
            kind: 'OB.UI.AcceptDialogButton',
            tap: function() {
                
                this.doHideThisPopup();
            }
        }]
    },
    executeOnShow: function() {
        console.log("Execute on show");
        var me = this;
        console.log(me);
        me.$.bodyContent.$.message1.setContent(this.args.description);
    },
    executeOnHide:function(){
        this.args.callback();
    },
    initComponents: function() {
        var me = this;
        this.inherited(arguments);
    }
});


OB.UI.WindowView.registerPopup('OB.OBPOSPointOfSale.UI.PointOfSale', {
    kind: 'ALPROM_DescripcionPromocion',
    name: 'ALPROM_DescripcionPromocion'
});