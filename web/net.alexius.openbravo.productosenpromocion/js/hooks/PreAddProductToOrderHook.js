OB.UTIL.HookManager.registerHook('OBPOS_PreAddProductToOrder', function(args, c) {
    
    if (Number(args.qtyToAdd) < 0) {
        //se esta eliminando el producto
        OB.UTIL.HookManager.callbackExecutor(args, c);
    } else {
        if(args.productToAdd.attributes.alprom_qty>0){
            var serverCall = new OB.DS.Process('net.alexius.openbravo.productosenpromocion.GetProductPromotions');
            serverCall.exec({
                idProducto: args.productToAdd.id
            },
            function(data) {
                if (data[0]) {
                    console.log('HAY DESCRIPCIONES');
                    console.log(data);
                    var titulos="";
                    var descripciones="";
                    for(var i=0; i<data.length;i++){
                        var aux="<strong>"+(i+1).toString()+")</strong> ";
                        var descripcion=data[i][1];
                        if(descripcion==null){
                            descripcion=data[i][0];
                        }
                        titulos+=aux+data[i][0]+"<br><hr>";
                        descripciones+=aux+descripcion+"<br><hr>";
                    }

                    OB.MobileApp.view.$.containerWindow.getRoot().doShowPopup({
                        popup: 'ALPROM_DescripcionPromocion',
                        args: {
                            nomProducto: args.productToAdd.attributes._identifier,
                            promociones: titulos,
                            description: descripciones,
                            callback:function(){
                                OB.UTIL.HookManager.callbackExecutor(args, c);
                            }
                        }
                    });
                } else {
                   //no hay descripciones seguramente la promocion esta deshabilitada
                    OB.UTIL.HookManager.callbackExecutor(args, c);
                }
            },
            function(error) {
                console.log('*** ERROR ****');
            });
        }else{
            OB.UTIL.HookManager.callbackExecutor(args, c);
        }
    }
});