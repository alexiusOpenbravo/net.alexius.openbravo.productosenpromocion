//OBPOS_RenderProduct
OB.UTIL.HookManager.registerHook('OBPOS_RenderProduct', function(args, callbacks) {
    var line=args.model;
    if(line.model.get("alprom_qty")>0){
        line.setStyle("background-color:rgba(106, 177, 62, 0.8);font-weight:bold;");
      }else if(line.model.get("alprom_qty")<0){
        line.setStyle("background-color:rgba(228, 215, 33, 0.8);font-weight:bold;");
      }
});